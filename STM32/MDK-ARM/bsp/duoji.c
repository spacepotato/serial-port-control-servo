#include "duoji.h"
//处理数据控制舵机角度
void duoji_set_angle(uint8_t data)
{
	float val = 2000.0 / 0xff * data;
	__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_1,(int)val+500);
}
//初始化舵机
void duoji_init()
{
	HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
	duoji_set_angle(0xff/2);
}


