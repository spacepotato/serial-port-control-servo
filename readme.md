# 串口控制舵机

# 简介

​	电脑通过QT上位机向32发送数据，32解析数据控制舵机角度。

# 视频演示

【QT上位机控制舵机】 https://www.bilibili.com/video/BV1gN4y1Z7AF/?share_source=copy_web&vd_source=4ba44f567e0b4eff7f7ab34041486613

# 使用模块

1. 主控使用STM32F407ZGT6。
2. 舵机使用sg90小蓝舵机。

# 引脚使用

​	1. PD12配置为PWM输出，连接舵机的信号线。

​	2. PA9和PA10作为串口1通过ch340模块连接电脑。