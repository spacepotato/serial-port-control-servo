/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_4;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QComboBox *portlist;
    QPushButton *reloadbutton;
    QPushButton *openbutton;
    QPushButton *closebutton;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout;
    QTextBrowser *datapool;
    QPushButton *clearbutton;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout_2;
    QSlider *mainval;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *startbutton;
    QPushButton *pausebutton;
    QWidget *widget_5;
    QVBoxLayout *verticalLayout_3;
    QWidget *widget_6;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QWidget *widget_7;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QWidget *widget_8;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(400, 800);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_4 = new QVBoxLayout(centralwidget);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        widget = new QWidget(centralwidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        portlist = new QComboBox(widget);
        portlist->setObjectName(QString::fromUtf8("portlist"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(portlist->sizePolicy().hasHeightForWidth());
        portlist->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(portlist);

        reloadbutton = new QPushButton(widget);
        reloadbutton->setObjectName(QString::fromUtf8("reloadbutton"));

        horizontalLayout->addWidget(reloadbutton);

        openbutton = new QPushButton(widget);
        openbutton->setObjectName(QString::fromUtf8("openbutton"));

        horizontalLayout->addWidget(openbutton);

        closebutton = new QPushButton(widget);
        closebutton->setObjectName(QString::fromUtf8("closebutton"));

        horizontalLayout->addWidget(closebutton);


        verticalLayout_4->addWidget(widget);

        widget_2 = new QWidget(centralwidget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        verticalLayout = new QVBoxLayout(widget_2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        datapool = new QTextBrowser(widget_2);
        datapool->setObjectName(QString::fromUtf8("datapool"));
        sizePolicy.setHeightForWidth(datapool->sizePolicy().hasHeightForWidth());
        datapool->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(datapool);

        clearbutton = new QPushButton(widget_2);
        clearbutton->setObjectName(QString::fromUtf8("clearbutton"));

        verticalLayout->addWidget(clearbutton);


        verticalLayout_4->addWidget(widget_2);

        widget_3 = new QWidget(centralwidget);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        verticalLayout_2 = new QVBoxLayout(widget_3);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        mainval = new QSlider(widget_3);
        mainval->setObjectName(QString::fromUtf8("mainval"));
        mainval->setMaximum(255);
        mainval->setValue(127);
        mainval->setOrientation(Qt::Horizontal);
        mainval->setTickPosition(QSlider::NoTicks);
        mainval->setTickInterval(0);

        verticalLayout_2->addWidget(mainval);

        widget_4 = new QWidget(widget_3);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        horizontalLayout_2 = new QHBoxLayout(widget_4);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        startbutton = new QPushButton(widget_4);
        startbutton->setObjectName(QString::fromUtf8("startbutton"));

        horizontalLayout_2->addWidget(startbutton);

        pausebutton = new QPushButton(widget_4);
        pausebutton->setObjectName(QString::fromUtf8("pausebutton"));

        horizontalLayout_2->addWidget(pausebutton);


        verticalLayout_2->addWidget(widget_4);


        verticalLayout_4->addWidget(widget_3);

        widget_5 = new QWidget(centralwidget);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        verticalLayout_3 = new QVBoxLayout(widget_5);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        widget_6 = new QWidget(widget_5);
        widget_6->setObjectName(QString::fromUtf8("widget_6"));
        horizontalLayout_3 = new QHBoxLayout(widget_6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pushButton = new QPushButton(widget_6);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_3->addWidget(pushButton);

        pushButton_2 = new QPushButton(widget_6);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_3->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(widget_6);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout_3->addWidget(pushButton_3);


        verticalLayout_3->addWidget(widget_6);

        widget_7 = new QWidget(widget_5);
        widget_7->setObjectName(QString::fromUtf8("widget_7"));
        horizontalLayout_4 = new QHBoxLayout(widget_7);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        pushButton_4 = new QPushButton(widget_7);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        horizontalLayout_4->addWidget(pushButton_4);

        pushButton_5 = new QPushButton(widget_7);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        horizontalLayout_4->addWidget(pushButton_5);

        pushButton_6 = new QPushButton(widget_7);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        horizontalLayout_4->addWidget(pushButton_6);


        verticalLayout_3->addWidget(widget_7);

        widget_8 = new QWidget(widget_5);
        widget_8->setObjectName(QString::fromUtf8("widget_8"));
        horizontalLayout_5 = new QHBoxLayout(widget_8);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_7 = new QPushButton(widget_8);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));

        horizontalLayout_5->addWidget(pushButton_7);

        pushButton_8 = new QPushButton(widget_8);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));

        horizontalLayout_5->addWidget(pushButton_8);

        pushButton_9 = new QPushButton(widget_8);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));

        horizontalLayout_5->addWidget(pushButton_9);


        verticalLayout_3->addWidget(widget_8);


        verticalLayout_4->addWidget(widget_5);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 400, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        reloadbutton->setText(QCoreApplication::translate("MainWindow", "\345\210\267\346\226\260", nullptr));
        openbutton->setText(QCoreApplication::translate("MainWindow", "\346\211\223\345\274\200", nullptr));
        closebutton->setText(QCoreApplication::translate("MainWindow", "\345\205\263\351\227\255", nullptr));
        clearbutton->setText(QCoreApplication::translate("MainWindow", "\346\270\205\351\231\244", nullptr));
        startbutton->setText(QCoreApplication::translate("MainWindow", "\345\274\200\345\247\213", nullptr));
        pausebutton->setText(QCoreApplication::translate("MainWindow", "\346\232\202\345\201\234", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "\345\210\235\345\247\213\345\214\226", nullptr));
        pushButton_2->setText(QCoreApplication::translate("MainWindow", "\350\257\273\345\215\241", nullptr));
        pushButton_3->setText(QCoreApplication::translate("MainWindow", "\345\257\206\351\222\245\351\252\214\350\257\201", nullptr));
        pushButton_4->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        pushButton_5->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        pushButton_6->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        pushButton_7->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        pushButton_8->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        pushButton_9->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
