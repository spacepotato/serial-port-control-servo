#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QSerialPort>
#include <QSerialPortInfo>
#include <qdebug.h>
#include <QTimer>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QList<QSerialPortInfo> Ports;//存放系统所有串口
    QTimer*  tim1;


    void Port_Scan();//扫描端口
    void send_data2duoji();//发送数据给舵机
private slots:

    void on_reloadbutton_clicked();

    void on_clearbutton_clicked();

    void on_openbutton_clicked();

    void receiveInfo();

    void on_closebutton_clicked();

    void on_startbutton_clicked();

    void on_pausebutton_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
