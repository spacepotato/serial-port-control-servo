#include "mainwindow.h"
#include "ui_mainwindow.h"

//扫描电脑的端口
void MainWindow::Port_Scan()
{
    Ports = QSerialPortInfo::availablePorts();
    //清空portlist
    ui->portlist->clear();
    for(int i = 0;i < Ports.size();i++)
    {
        ui->datapool->append(QString("端口%1:").arg(i));
        ui->datapool->append(QString(Ports.at(i).portName()));
        ui->datapool->append(QString(Ports.at(i).description()));
        //填入portlist
        ui->portlist->addItem(Ports.at(i).portName());

    }
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //定时器1
    tim1 = new QTimer(this);
    QObject::connect(tim1, &QTimer::timeout,this, [=]()
    {
        send_data2duoji();
    });

    ui->closebutton->setEnabled(false);

    ui->pausebutton->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

//刷新按钮
void MainWindow::on_reloadbutton_clicked()
{
    Port_Scan();
}
//清除数据浏览框数据
void MainWindow::on_clearbutton_clicked()
{
    ui->datapool->clear();
}
//打开串口
QSerialPort *selport = new QSerialPort();//实例化串口类一个对象
void MainWindow::on_openbutton_clicked()
{
    if(selport->isOpen())
    {
        selport->clear();
        selport->close();
    }

    //设置串口名称
    selport->setPortName(ui->portlist->currentText());


    if(!selport->open(QIODevice::ReadWrite))//用ReadWrite 的模式尝试打开串口
    {
        ui->datapool->append(QString("打开失败!"));
        QMessageBox::critical(this, tr("错误"), tr("串口打开失败！"));

        return;
    }else
    {
         ui->datapool->append(QString("打开成功!"));
         ui->closebutton->setEnabled(true);
         ui->openbutton->setEnabled(false);
    }
    //打开成功
    selport->setBaudRate(QSerialPort::Baud115200,QSerialPort::AllDirections);//设置波特率和读写方向
    selport->setDataBits(QSerialPort::Data8);		//数据位为8位
    selport->setFlowControl(QSerialPort::NoFlowControl);//无流控制
    selport->setParity(QSerialPort::NoParity);	//无校验位
    selport->setStopBits(QSerialPort::OneStop); //一位停止位

    //连接信号槽 当下位机发送数据QSerialPortInfo 会发送个 readyRead 信号,我们定义个槽void receiveInfo()解析数据
    connect(selport,SIGNAL(readyRead()),this,SLOT(receiveInfo()));
}
//处理串口数据
void MainWindow::receiveInfo()
{
    QByteArray data = selport->readAll();
    ui->datapool->append(QString(data));
}

//关闭串口
void MainWindow::on_closebutton_clicked()
{
    selport->clear();
    selport->close();
    if(selport->isOpen())
    {
        ui->datapool->append(QString("关闭失败!"));
    }else
    {
        QMessageBox::critical(this, tr("错误"), tr("串口关闭失败！"));
        ui->datapool->append(QString("关闭成功!"));
        ui->closebutton->setEnabled(false);
        ui->openbutton->setEnabled(true);
    }
}
//把数据发给舵机
void MainWindow::send_data2duoji()
{
    uint8_t buffer[1] = {255 - (uint8_t)ui->mainval->value()};
    qDebug() << buffer[0] << endl;
    selport->write((char*)buffer , 1);
}

//开始串口数据发送
void MainWindow::on_startbutton_clicked()
{
    if(!selport->isOpen())
    {
        QMessageBox::critical(this, tr("错误"), tr("串口未打开！"));
        return;
    }
    tim1->start(10);

    ui->pausebutton->setEnabled(true);
    ui->startbutton->setEnabled(false);
}

//暂停串口数据发送
void MainWindow::on_pausebutton_clicked()
{
    tim1->stop();
    ui->pausebutton->setEnabled(false);
    ui->startbutton->setEnabled(true);
}
