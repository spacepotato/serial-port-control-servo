/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_3;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QComboBox *portlist;
    QPushButton *reloadbutton;
    QPushButton *openbutton;
    QPushButton *closebutton;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout;
    QTextBrowser *datapool;
    QPushButton *clearbutton;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout_2;
    QSlider *mainval;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *startbutton;
    QPushButton *pausebutton;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(400, 800);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_3 = new QVBoxLayout(centralwidget);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        widget = new QWidget(centralwidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        portlist = new QComboBox(widget);
        portlist->setObjectName(QString::fromUtf8("portlist"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(portlist->sizePolicy().hasHeightForWidth());
        portlist->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(portlist);

        reloadbutton = new QPushButton(widget);
        reloadbutton->setObjectName(QString::fromUtf8("reloadbutton"));

        horizontalLayout->addWidget(reloadbutton);

        openbutton = new QPushButton(widget);
        openbutton->setObjectName(QString::fromUtf8("openbutton"));

        horizontalLayout->addWidget(openbutton);

        closebutton = new QPushButton(widget);
        closebutton->setObjectName(QString::fromUtf8("closebutton"));

        horizontalLayout->addWidget(closebutton);


        verticalLayout_3->addWidget(widget);

        widget_2 = new QWidget(centralwidget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        verticalLayout = new QVBoxLayout(widget_2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        datapool = new QTextBrowser(widget_2);
        datapool->setObjectName(QString::fromUtf8("datapool"));

        verticalLayout->addWidget(datapool);

        clearbutton = new QPushButton(widget_2);
        clearbutton->setObjectName(QString::fromUtf8("clearbutton"));

        verticalLayout->addWidget(clearbutton);


        verticalLayout_3->addWidget(widget_2);

        widget_3 = new QWidget(centralwidget);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        verticalLayout_2 = new QVBoxLayout(widget_3);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        mainval = new QSlider(widget_3);
        mainval->setObjectName(QString::fromUtf8("mainval"));
        mainval->setMaximum(255);
        mainval->setValue(127);
        mainval->setOrientation(Qt::Horizontal);
        mainval->setTickPosition(QSlider::NoTicks);
        mainval->setTickInterval(0);

        verticalLayout_2->addWidget(mainval);

        widget_4 = new QWidget(widget_3);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        horizontalLayout_2 = new QHBoxLayout(widget_4);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        startbutton = new QPushButton(widget_4);
        startbutton->setObjectName(QString::fromUtf8("startbutton"));

        horizontalLayout_2->addWidget(startbutton);

        pausebutton = new QPushButton(widget_4);
        pausebutton->setObjectName(QString::fromUtf8("pausebutton"));

        horizontalLayout_2->addWidget(pausebutton);


        verticalLayout_2->addWidget(widget_4);


        verticalLayout_3->addWidget(widget_3);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 400, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        reloadbutton->setText(QCoreApplication::translate("MainWindow", "\345\210\267\346\226\260", nullptr));
        openbutton->setText(QCoreApplication::translate("MainWindow", "\346\211\223\345\274\200", nullptr));
        closebutton->setText(QCoreApplication::translate("MainWindow", "\345\205\263\351\227\255", nullptr));
        clearbutton->setText(QCoreApplication::translate("MainWindow", "\346\270\205\351\231\244", nullptr));
        startbutton->setText(QCoreApplication::translate("MainWindow", "\345\274\200\345\247\213", nullptr));
        pausebutton->setText(QCoreApplication::translate("MainWindow", "\346\232\202\345\201\234", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
